import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {NotfoundComponent} from './notfound/notfound.component';
import {UnauthorizedComponent} from './unauthorized/unauthorized.component';
import {HomeComponent} from './home/home.component';
import {OnibusListagemComponent} from './onibus-listagem/onibus-listagem.component';
import {LotacaoListagemComponent} from './lotacao-listagem/lotacao-listagem.component';
import {ItinerarioComponent} from './itinerario/itinerario.component';

const routes: Routes = [
  {
    path: 'home',
    redirectTo: '',
    pathMatch: 'full'
  },

  {
    path: 'onibus-listagem',
    data: {
      breadcrumb: 'Listagem de Ônibus'
    },
    children: [
      {
        path: '',
        component: OnibusListagemComponent,
      },
      {
        path: 'itinerario/:id',
        component: ItinerarioComponent,
        data: {
          breadcrumb: 'Detalhes do itinerário'
        }
      }
    ]
  },

  {
    path: 'lotacao-listagem',
    data: {
      breadcrumb: 'Listagem de Lotações'
    },
    children: [
      {
        path: '',
        component: LotacaoListagemComponent,
      },
      {
        path: 'itinerario/:id',
        component: ItinerarioComponent,
        data: {
          breadcrumb: 'Detalhes do itinerário'
        }
      }
    ]
  },
  {
    path: 'itinerario/:id',
    component: ItinerarioComponent,
    data: {
      breadcrumb: 'Detalhes do itinerário'
    }
  },
  {
    path: '',
    component: HomeComponent,
    data: {
      breadcrumb: 'Página Inicial'
    }
  },
  {
    path: 'unauthorized',
    component: UnauthorizedComponent,
    data: {
      breadcrumb: 'Usuário não autorizado'
    }
  },
  {
    path: '**',
    component: NotfoundComponent,
    data: {
      breadcrumb: 'Página Não Encontrada'
    }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
