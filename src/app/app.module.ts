import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {OnibusListagemModule } from './onibus-listagem/onibus-listagem.module';
import {LotacaoListagemComponent} from './lotacao-listagem/lotacao-listagem.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { HomeComponent } from './home/home.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import {RouterModule} from '@angular/router';
import {ItinerariosService} from './services/itinerarios.service';
import {LotacaoListagemService} from './services/lotacao-listagem.service';
import { LoadingModule } from './loading/loading.module';
import { HeaderModule } from './header/header.module';


@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    UnauthorizedComponent,
    HomeComponent,
    LotacaoListagemComponent,
    ItinerarioComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    AppRoutingModule,
    LoadingModule,
    HeaderModule,
    OnibusListagemModule
  ],
  providers: [
    ItinerariosService,
    LotacaoListagemService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
