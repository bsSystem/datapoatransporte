import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from '@angular/router';
import {ItinerariosService} from '../services/itinerarios.service';
import {ItinerariolatlogModel} from '../model/itinerariolatlog.model';
import {ItinerarioModel} from '../model/itinerario.model';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {
  codigo: any;
  objItinerario: ItinerarioModel;
  obj: any[];
  objLocalidades: any[];
  pageCurrent: any;


  constructor(
    private activatedRoute: ActivatedRoute,
    private itinerariosService: ItinerariosService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.objLocalidades = [];
    this.pageCurrent = this.activatedRoute.snapshot['_routerState'].url.split('/')[1];

    this.activatedRoute.params.subscribe(params => {
      console.log(params);
      this.codigo = params['id'];
      this.getItinerarios(this.codigo);
    });
  }


  getItinerarios(id: number) {
    this.itinerariosService.getIntinerarioById(id).subscribe(
      data => {
        this.obj = [];
        this.obj.push(<ItinerarioModel>data);

        for (let i = 0; i < this.obj.length; i++) {
          for (let j = 0; j < 10000; j++) {
            if (this.obj[i][j] !== undefined) {
              this.objLocalidades.push(this.obj[i][j]);
            } else {
              break;
            }
          }
        }
        this.objItinerario = <ItinerarioModel>data;
      }, erro => {
        console.log(erro);
      });
  }

}
