import { Component, OnInit } from '@angular/core';
import {LotacaoListagemService} from '../services/lotacao-listagem.service';
import {ItemLotacaoModel} from '../model/item-lotacao.model';

@Component({
  selector: 'app-lotacao-listagem',
  templateUrl: './lotacao-listagem.component.html',
  styleUrls: ['./lotacao-listagem.component.css']
})
export class LotacaoListagemComponent implements OnInit {
  public exibeLista = false;
  public lotacaoItens: ItemLotacaoModel[];
  public searchText: string;
  emptyList: boolean;
  showLoading: boolean;

  constructor(
    private lotacaoListagemService: LotacaoListagemService
  ) { }

  ngOnInit() {
  }

  buscarItens(lotacaoNome?: string) {
    this.showLoading = true;

    this.lotacaoListagemService.buscarLotacaoListagem(lotacaoNome)
      .subscribe(
        res => {
          this.lotacaoItens = res;
          if (res.length > 0) {
            this.emptyList = false;
            this.exibeLista = true;
          } else {
            this.emptyList = true;
            this.exibeLista = false;
          }
          this.showLoading = false;
        },
        err => {
          this.showLoading = false;
          this.emptyList = true;
          this.exibeLista = false;
        }
      );
  }

  buscarLotacao(ev: any) {
    if (ev !== '' && ev.length >= 3) {
      this.buscarItens(ev);
    } else {
      this.exibeLista = false;
      this.emptyList = false;
    }
  }
}
