import {ItinerariolatlogModel} from './itinerariolatlog.model';

export class ItinerarioModel {

  idlinha: string;
  codigo: string;
  nome: string;
  itinerario: ItinerariolatlogModel[];


  public constructor(idlinha: string,
              codigo: string,
              nome: string,
              itinerario: ItinerariolatlogModel[]
  ) {

    this.idlinha = idlinha;
    this.codigo = codigo;
    this.nome = nome;
    this.itinerario = itinerario;

  }
}
