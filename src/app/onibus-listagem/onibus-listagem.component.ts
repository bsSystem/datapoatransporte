import { Component, OnInit } from '@angular/core';
import { ItemOnibusModel } from './model/item-onibus.model';
import { OnibusListagemService } from './service/onibus-listagem.service';


@Component({
  selector: 'app-onibus-listagem',
  templateUrl: './onibus-listagem.component.html',
  styleUrls: ['./onibus-listagem.component.css']
})
export class OnibusListagemComponent implements OnInit {

  public onibusNome: string;
  public exibeLista = false;
  public onibusItens: ItemOnibusModel[];
  public searchText: string;
  emptyList: boolean;
  showLoading: boolean;

  constructor(
    private onibusListagemService: OnibusListagemService
  ) { }

  ngOnInit() {
    this.showLoading = false;
  }

  buscarItens(onibusNome?: string) {
    this.showLoading = true;

    this.onibusListagemService.buscarOnibusListagem(onibusNome)
        .subscribe(
            res => {
                this.onibusItens = res;
                if (res.length > 0) {
                  this.emptyList = false;
                  this.exibeLista = true;
                } else {
                  this.emptyList = true;
                  this.exibeLista = false;
                }

              this.showLoading = false;
            },
            err => {
                this.showLoading = false;

                this.emptyList = true;
                this.exibeLista = false;
            }
        );
  }

  buscarBus(ev: any) {
    if (ev !== '' && ev.length >= 3) {
      this.buscarItens(ev);
    } else {
      this.exibeLista = false;
      this.emptyList = false;
    }
  }
}
