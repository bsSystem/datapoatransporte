import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ItinerarioModel} from '../model/itinerario.model';

@Injectable({
  providedIn: 'root'
})
export class ItinerariosService {

  constructor(public http: HttpClient) { }


  getIntinerarioById(id: number) {
    return this.http.get<ItinerarioModel>( `${environment.itinerarioApiUrl}&p=${id}`);
  }
}
