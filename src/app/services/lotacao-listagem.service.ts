import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { ItemLotacaoModel } from '../model/item-lotacao.model';

@Injectable({
  providedIn: 'root'
})
export class LotacaoListagemService {

  constructor(public http: HttpClient) { }


  buscarLotacaoListagem(itemLotacao: string) {
    return this.http.get<ItemLotacaoModel[]>( `${environment.lotacaoApiUrl}&p=${itemLotacao}%&t=l`);
  }
}
